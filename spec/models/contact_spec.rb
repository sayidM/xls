require 'rails_helper'

RSpec.describe Contact, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.build(:contact)).to be_valid
  end

  it "belongs to an upload" do
    expect(FactoryGirl.build(:contact, upload_id: nil)).not_to be_valid
  end

  it "has a unique last name/first name" do
    last_name = 'Gates'
    first_name = 'Bill'

    # First time should work
    expect(FactoryGirl.create(:contact, last_name: last_name, first_name: first_name)).to be_valid
    
    # Second time should fail b/c couple last_name/first_name already present in the DB
    expect(FactoryGirl.build(:contact, last_name: last_name, first_name: first_name)).not_to be_valid
  end

  it "has a unique email" do
    email = 'bill@gates.com'

    expect(FactoryGirl.create(:contact, email: email)).to be_valid
    expect(FactoryGirl.build(:contact, email: email)).not_to be_valid
  end
  
  it "has a valid email" do
    email = 'bill'

    expect(FactoryGirl.build(:contact, email: email)).not_to be_valid
  end
  
  it "has at least 3 letters in the last name" do
    last_name = 'ab'
    expect(FactoryGirl.build(:contact, last_name: last_name)).not_to be_valid
  end
  
  it "has at least 3 letters in the first name" do
    first_name = 'c'
    expect(FactoryGirl.build(:contact, first_name: first_name)).not_to be_valid
  end

  it "has no incorrect chars" do
    first_name = 'Saïd'
    
    expect(FactoryGirl.create(:contact, first_name: first_name)).to be_valid
    expect(Contact.last.first_name).to eq 'Said' # It shoud remove the 'ï'
  end
end

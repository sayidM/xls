FactoryGirl.define do
  factory :contact do
    upload_id 1
    sequence(:last_name) { |n| "Last name #{n}" }    
    sequence(:first_name) { |n| "First name #{n}" }    
    sequence(:email) { |n| "#{n}@email.com" }    
  end
end

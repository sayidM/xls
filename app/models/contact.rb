require 'i18n'

class Contact < ActiveRecord::Base
  belongs_to :upload

  validates :upload_id, presence: true
  validates :first_name, presence: true, uniqueness: true, length: { minimum: 3 }
  validates :last_name, presence: true, uniqueness: true, length: { minimum: 3 }
  validates :email, presence: true, uniqueness: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }

  before_validation :transliterate

  private
  def transliterate
    self.email = I18n.transliterate(self.email)
    self.first_name = I18n.transliterate(self.first_name)
    self.last_name = I18n.transliterate(self.last_name)
  end
end

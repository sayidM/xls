class Upload < ActiveRecord::Base
  attr_accessor :file
  has_many :contacts

  after_create :create_contacts

  private
    def create_contacts
      spreadsheet = open_spreadsheet(file)
      (2..spreadsheet.last_row).each do |i| # We don't care about the header
        row = spreadsheet.row(i)

        first_name = row[0]
        last_name = row[1]
        email = row[2]

        contact = self.contacts.new(first_name: first_name, last_name: last_name, email: email)
        unless contact.save
          # Add the errors to an array and display it
          errors.add(:base, contact.errors.messages.first)
        end
      end
    end
    
    def open_spreadsheet(file)
      case File.extname(file.original_filename)
      when ".xls" then Excel.new(self.file.path, nil: ignore)
      when ".xlsx" then Roo::Excelx.new(file.path)
      else raise "Unknown file type"
      end
    end
end

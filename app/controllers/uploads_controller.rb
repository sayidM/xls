class UploadsController < ApplicationController
  def index
    @uploads = Upload.includes(:contacts).all
  end

  def show
    @upload = Upload.find params[:id]
  end

  def new
    @upload = Upload.new
  end

  def create
    @upload = Upload.new upload_params

    unless @upload.save
      render text: @upload.errors.first
    end
  end

  private
    def upload_params
      params.require(:upload).permit(:file)
    end
end

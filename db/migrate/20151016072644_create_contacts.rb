class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.belongs_to :upload, null: false, index: true
      t.string :last_name, null: false
      t.string :first_name, null: false
      t.string :email, null: false
    end

    add_index :contacts, [:last_name, :first_name], unique: true # Make the couple last_name/first_name unique
    add_index :contacts, :email, unique: true
  end
end
